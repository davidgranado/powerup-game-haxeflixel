package hud;

import entities.Player;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class Hud extends FlxSpriteGroup {
	public var powerBar : Bar;
	public var powerText : FlxText;
	public var lifeBar : Bar;
	public var lifeText : FlxText;
	public var time = 0.0;
	public var timeText : FlxText;

	var player : Player;

	public function new(_player : Player) {
		super();

		scrollFactor.set(0, 0);
		alpha = 0.7;

		player = _player;

		var fontSize = 18;
		var width = 95;

		powerText = new FlxText(0, 8, width, 'Power:', fontSize);
		lifeText = new FlxText(0, 32, width, 'Life:', fontSize);
		timeText = new FlxText(FlxG.width - width, 8, width, 'Time:', fontSize);

		lifeText.alignment = FlxTextAlign.RIGHT;
		powerText.alignment = FlxTextAlign.RIGHT;
		timeText.origin.set(FlxG.width);

		lifeText.size = timeText.size = powerText.size = 18;
		lifeText.color = timeText.color = powerText.color = FlxColor.BLACK;

		add(lifeText);
		add(timeText);
		add(powerText);

		powerBar = new Bar(100, 10, 500, FlxColor.RED);
		lifeBar = new Bar(100, 34, 500, FlxColor.GREEN);

		powerBar.setScale(0);

		add(powerBar);
		add(lifeBar);
	}

	override public function update(dt : Float) : Void {
		time += dt;

		super.update(dt);
	}

	public function updateLifeScale(scale : Float) {
		lifeBar.setScale(scale);
	}

	public function updatePowerScale(scale : Float) {
		powerBar.setScale(scale);
	}
}
