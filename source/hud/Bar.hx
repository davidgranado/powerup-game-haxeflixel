package hud;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.util.FlxColor;

class Bar extends FlxSpriteGroup {
	static var HEIGHT = 24;

	var background : FlxSprite;
	var foreground : FlxSprite;

	public function new(x : Float, y : Float, width : Int, color : FlxColor) {
		super(x, y);

		background = new FlxSprite();
		foreground = new FlxSprite();

		foreground.x = 1;
		foreground.y = 1;

		background.makeGraphic(width, HEIGHT, FlxColor.BLACK);
		foreground.makeGraphic(width, HEIGHT - 2, color);

		background.origin.set(0);
		foreground.origin.set(0);

		add(background);
		add(foreground);
	}

	public function setScale(scale : Float) {
		foreground.scale.x = scale;
	}
}
