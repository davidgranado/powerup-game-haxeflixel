package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.math.FlxMath;

enum Buttons {
	Play;

	#if !(js || flash)
		Quit;
	#end
}

class MenuState extends FlxState {
	var fontWidth = 400.0;
	var playButton : FlxText;
	var quitButton : FlxText;
	var verticalOffset = 120.0;
	var buttons : Map<Buttons, FlxText>;
	var activeButton : Buttons;

	override public function create():Void {
		super.create();

		buttons = new Map<Buttons, FlxText>();

		activeButton = Buttons.Play;

		var centerX = (FlxG.width - fontWidth) / 2;
		var title = new FlxText(centerX, verticalOffset, fontWidth, "Power Prototype");

		title.alignment = CENTER;
		title.size = 35;

		add(title);

		FlxG.camera.bgColor = FlxColor.BLUE;

		playButton = new FlxText(centerX, verticalOffset + 100, fontWidth, "Play");
		playButton.size = 25;
		playButton.alignment = CENTER;
		playButton.color = FlxColor.RED;
		buttons[Buttons.Play] = playButton;

		add(playButton);

		#if !(js || flash)
			quitButton = new FlxText(centerX, verticalOffset + 150, fontWidth, "Quit");
			quitButton.size = 25;
			quitButton.alignment = CENTER;
			buttons[Buttons.Quit] = quitButton;

			add(quitButton);
		#end
	}

	override public function update(elapsed:Float):Void
	{
		updateInput();

		super.update(elapsed);
	}

	public function startPlay() {
		FlxG.switchState(new PlayState());
	}

	#if !(js || flash)
		public function quitGame() {
			Sys.exit(0);
		}
	#end

	public function updateInput() {

		if(FlxG.gamepads.lastActive == null) {
			if(
				FlxG.keys.justPressed.UP ||
				FlxG.keys.justPressed.DOWN
			) {
				buttons[activeButton].color = FlxColor.WHITE;

				#if !(js || flash)
					activeButton = activeButton == Buttons.Play ? Buttons.Quit : Buttons.Play;
				#end
				buttons[activeButton].color = FlxColor.RED;
			}

			if(FlxG.keys.justPressed.ENTER) {
				switch(activeButton) {
					case Buttons.Play: startPlay();
					#if !(js || flash)
						case Buttons.Quit: quitGame();
					#end
				}
			}
		} else {
			if(FlxG.gamepads.lastActive.analog.justMoved.LEFT_STICK_Y) {
				buttons[activeButton].color = FlxColor.WHITE;

				#if !(js || flash)
					activeButton = activeButton == Buttons.Play ? Buttons.Quit : Buttons.Play;
				#end
				buttons[activeButton].color = FlxColor.RED;
			}

			if(FlxG.gamepads.lastActive.justPressed.A) {
				switch(activeButton) {
					case Buttons.Play: startPlay();
					#if !(js || flash)
						case Buttons.Quit: quitGame();
					#end
				}
			}
		}
	}
}
