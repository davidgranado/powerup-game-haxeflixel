package entities;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxAngle;

class Powerup extends FlxSprite {
	var aggressiveness : Float;
	var size = 10;
	var speed = 500;

	public function new(_x, _y) {
		super(_x, _y);
		makeGraphic( size, size, FlxColor.PURPLE);

		cast(FlxG.state, PlayState).powerups.add(this);
	}

	override public function update(dt : Float) : Void {
		var angle = FlxAngle.angleBetween(this, cast (FlxG.state, PlayState).player);

		velocity.x = speed * Math.cos(angle);
		velocity.y = speed * Math.sin(angle);

		super.update(dt);
	}
}
