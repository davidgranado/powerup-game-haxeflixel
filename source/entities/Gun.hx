package entities;

import flixel.FlxG;
import flixel.FlxBasic;
import flixel.FlxObject;

class Gun extends FlxBasic{
	public var facing : Int;
	public var x : Float;
	public var y : Float;
	public var variability = 150.0;
	public var power = 1;
	public var collectedPowerups = 1;
	var powerInterval = 100;

	var cooldownTime : Float = 0.2;
	var remainingCooldown : Float = 0.0;

	public function new(_x : Float, _y : Float) {
		super();

		x = _x;
		y = _y;
	}

	override public function update(dt : Float) : Void {
		super.update(dt);

		if (remainingCooldown > 0) {
			remainingCooldown -= dt;
		} else {
			updateInput();
		}
	}

	public function collectPowerup() {
		collectedPowerups++;

		if(collectedPowerups % powerInterval == 0) {
			power++;
		}
	}

	public function updateInput() {
		var horizontal = 0.0;
		var vertical = 0.0;

		if(FlxG.gamepads.lastActive == null) {
			if(FlxG.keys.pressed.UP) {
				vertical = -1.0;
			} else if(FlxG.keys.pressed.DOWN) {
				vertical = 1.0;
			}

			if(FlxG.keys.pressed.LEFT) {
				horizontal = -1.0;
			} else if(FlxG.keys.pressed.RIGHT) {
				horizontal = 1.0;
			}
		} else  if (
		   FlxG.gamepads.lastActive.analog.value.RIGHT_STICK_Y != 0 ||
		   FlxG.gamepads.lastActive.analog.value.RIGHT_STICK_X != 0
	   ) {
			vertical = FlxG.gamepads.lastActive.analog.value.RIGHT_STICK_Y;
			horizontal = FlxG.gamepads.lastActive.analog.value.RIGHT_STICK_X;
	   }

	   if(!(horizontal == 0 && vertical == 0)) {
		   new Bullet(x, y, horizontal, vertical, variability);
		   remainingCooldown = cooldownTime / power;
	   }
	}
}
