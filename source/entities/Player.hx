package entities;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxColor;
import flixel.FlxSprite;

class Player extends FlxSprite {
	public var dragVal = 700;
	public var flying = false;
	public var gravity = 500;
	public var gun : Gun;
	public var life = 100;
	public var maxSpeed = 750;
	public var maxStableLife = 1;
	public var maxStabelPower = 1;
	public var power = 0;
	public var speed = 1200;

	var gunOffsetHorizontal : Float;
	var gunOffsetVertical : Float;

	public function new(_x, _y) {
		super(_x, _y);

		makeGraphic(48, 64, FlxColor.YELLOW);

		maxVelocity.set(maxSpeed, maxSpeed);
		drag.set(dragVal, dragVal);

		gunOffsetHorizontal = width / 2;
		gunOffsetVertical = height / 3;
		acceleration.y = gravity;

		gun = new Gun(_x + gunOffsetHorizontal, _y + gunOffsetVertical);
	}

	override public function update(dt : Float) : Void {

		updateInput();

		if (x < 0) {
			velocity.x = 0;
			x = 0;
		} else if (x > FlxG.worldBounds.width - width) {
			velocity.x = 0;
			x = FlxG.worldBounds.width - width;
		}

		gun.x = x + gunOffsetHorizontal;
		gun.y = y + gunOffsetVertical;
		gun.facing = facing;

		FlxG.state.add(gun);

		super.update(dt);
	}

	public function toggleFlight() {
		flying = !flying;

		if(flying) {
			acceleration.y = 0;
		} else {
			acceleration.y = gravity;
		}
	}

	public function updateInput() {
		if(FlxG.gamepads.lastActive == null) {
			if(FlxG.keys.justPressed.Q) {
				toggleFlight();
			}

			if(FlxG.keys.pressed.D) {
				acceleration.x = speed;
			} else if(FlxG.keys.pressed.A) {
				acceleration.x = -speed;
			} else {
				acceleration.x = 0;
			}

			if(flying) {
				if(FlxG.keys.pressed.S) {
					acceleration.y = speed;
				} else if(FlxG.keys.pressed.W) {
					acceleration.y = -speed;
				} else {
					acceleration.y = 0;
				}
			}
		} else  {
			if(FlxG.gamepads.lastActive.justPressed.RIGHT_SHOULDER) {
				toggleFlight();
			}

			acceleration.x = speed * FlxG.gamepads.lastActive.analog.value.LEFT_STICK_X;

			if(flying) {
				acceleration.y = speed * FlxG.gamepads.lastActive.analog.value.LEFT_STICK_Y;
			}
		}
	}
}
