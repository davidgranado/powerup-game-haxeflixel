package entities;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxAngle;
import flixel.util.FlxTimer;

class EnemyPortal extends FlxSprite {
	var growthRate = 30.0;
	var teleportTime = 3;
	var portalClosing = false;
	var portalPadding = 50;
	var portalSize : Int;

	public function new(_x, _y, _portalSize) {
		super(_x, _y);
		portalSize = _portalSize;

		makeGraphic( 50, 1, FlxColor.LIME );

		cast(FlxG.state, PlayState).portals.add(this);
	}

	override public function update(dt : Float) : Void {

		if(portalClosing) {
			if(height > 0) {
				scale.y -= growthRate * dt;

				if(scale.y <= 0) {
					kill();
				}
			}
		} else if(scale.y * height < width) {
			scale.y = Math.min(scale.y + growthRate * dt, width);

			if(scale.y * height == width) {
				(new FlxTimer()).start(teleportTime, teleport);
			}
		}

		super.update(dt);
	}

	function teleport(timer: FlxTimer) : Void {
		portalClosing = true;

		for(i in 0...portalSize) {
			var posX = (Math.random() - 1) * (width + portalPadding);
			var posY = (Math.random() - 1) * (width + portalPadding);

			new Enemy(x + posX, y + posY);
		}

		timer.destroy();
	}
}
