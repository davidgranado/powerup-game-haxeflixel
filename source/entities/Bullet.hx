package entities;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;

using flixel.util.FlxSpriteUtil;

class Bullet extends FlxSprite {
	public var speed = 1000;
	public var lifetime = 0.75;

	public function new(_x : Float, _y : Float, horizontalDrection : Float, verticalDrection : Float, variability : Float) {
		super(_x, _y);

		velocity.x = speed * horizontalDrection + (Math.random() - 1 / 2) * variability;
		velocity.y = speed * verticalDrection + (Math.random() - 1 / 2) * variability;

		makeGraphic(10, 10, FlxColor.RED);

		(new FlxTimer()).start(lifetime, detonate);

		cast(FlxG.state, PlayState).bullets.add(this);
	}

	override public function update(dt : Float) : Void {
		super.update(dt);
	}

	public function detonate(timer: FlxTimer) : Void {
		destroy();
	}
}
