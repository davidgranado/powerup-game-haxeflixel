package entities;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxObject;
import flixel.FlxSprite;

class Ground extends FlxSprite {
	var thickness = 50;

	public function new(levelWidth, levelHeight) {

		super(0, Std.int( levelHeight - thickness ));
		makeGraphic( levelWidth, thickness, FlxColor.BROWN, true );
		immovable = true;
		solid = true;
		moves = false;
		allowCollisions =  FlxObject.UP;
	}
}
