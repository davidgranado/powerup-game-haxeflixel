package entities;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;

class Cloud extends FlxSprite {
	static var SPEED_RANGE = 20;
	static var MIN_SPEED = 10;

	var floor = 300;
	var heightRange = 100;
	var widthRange = 300;
	var minHeight = 50;
	var minWidth = 100;

	public function new() {
		super(
			Std.int( Math.random() * FlxG.worldBounds.width ),
			Std.int( Math.random() * ( FlxG.worldBounds.height - floor ) )
		);

		makeGraphic(
			Std.int( Math.random() * widthRange ) + minWidth,
			Std.int( Math.random() * heightRange ) + minHeight,
			FlxColor.WHITE
		);

		velocity.x = Std.int(Math.random() * Cloud.SPEED_RANGE) + MIN_SPEED;
	}

	override public function update(dt : Float) : Void {
		if(x >= FlxG.worldBounds.width) {
			x = -width;
		}

		super.update(dt);
	}
}
