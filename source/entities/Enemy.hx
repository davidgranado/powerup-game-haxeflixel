package entities;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxAngle;

class Enemy extends FlxSprite {
	var aggressiveness : Float;
	var size = 50;
	var speed = 300.0;
	var speedVariability = 50.0;
	var powerupValue = 10;

	public function new(x, y) {
		super(x, y);
		makeGraphic( size, size, FlxColor.RED );

		speed += (Math.random() - 1) * speedVariability;

		cast(FlxG.state, PlayState).enemies.add(this);
	}

	override public function update(dt : Float) : Void {
		var angle = FlxAngle.angleBetween(this, cast (FlxG.state, PlayState).player);

		velocity.x = speed * Math.cos(angle);
		velocity.y = speed * Math.sin(angle);

		super.update(dt);
	}

	override public function kill() : Void {
		// var powerupCount = Std.int(Math.random() * powerupValue);

		// TODO Spread powerups to make multiple
		// for(i in 0...powerupCount) {
			new Powerup(x, y);
		// }

		super.kill();
	}
}
