package controllers;

import entities.EnemyPortal;
import flixel.FlxBasic;
import flixel.FlxG;

class EnemyController extends FlxBasic {
	public var baseSpawnInterval = 10;
	public var baseSpawnCount = 10;
	public var remainingSpawnTime = 0.0;

	override public function update(dt : Float) {
		if(remainingSpawnTime <= 0) {
			var level = cast (FlxG.state, PlayState).level;

			new EnemyPortal(
				Math.random() * FlxG.worldBounds.width,
				Math.random() * FlxG.worldBounds.height,
				Std.int(baseSpawnCount * level)
			);

			remainingSpawnTime = baseSpawnInterval/level;
		} else {
			remainingSpawnTime -= dt;
		}
	}
}
