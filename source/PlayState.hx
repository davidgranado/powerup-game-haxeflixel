package;

import controllers.*;
import entities.*;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxSpriteGroup;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.math.FlxMath;
import flixel.math.FlxRect;
import hud.Hud;

class PlayState extends FlxState {
	public var bullets : FlxSpriteGroup;
	public var enemies : FlxSpriteGroup;
	public var player : Player;
	public var portals : FlxSpriteGroup;
	public var powerups : FlxSpriteGroup;
	public var hud : Hud;
	public var level = 1.0;

	var background :  FlxSpriteGroup;
	var ground : Ground;
	var deadzone : Float = 100.0;
	var levelHeight = 1000;
	var levelWidth = 2500;

	override public function create():Void {
		super.create();

		background = new FlxSpriteGroup();
		bullets = new FlxSpriteGroup();
		enemies = new FlxSpriteGroup();
		powerups = new FlxSpriteGroup();
		portals = new FlxSpriteGroup();

		add(background);
		add(portals);
		add(bullets);
		add(enemies);

		player = new Player(FlxG.worldBounds.width/2, FlxG.worldBounds.height - 200);
		add(player);

		hud = new Hud(player);

		camera.bgColor =  FlxColor.fromRGB(108, 140, 185);

		FlxG.worldBounds.setSize(levelWidth, levelHeight);
		camera.target = player;
		camera.followLead.set(10, 10);
		camera.followLerp = 0.1;
		camera.deadzone = new FlxRect((camera.width - deadzone) / 2, (camera.height - deadzone) / 2, deadzone, deadzone);
		camera.setScrollBoundsRect(0, 0, levelWidth, levelHeight);
		ground = new Ground(levelWidth, levelHeight);

		add(ground);
		add(new EnemyController());

		createClouds(16);

		FlxG.camera.antialiasing = false;

		add(powerups);
		add(hud);
	}

	override public function update(dt : Float) : Void {
		FlxG.collide(ground, player);
		FlxG.collide(enemies, player, killPlayer);
		FlxG.collide(enemies, bullets, killBoth);
		FlxG.overlap(powerups, player, collectPowerup);

		super.update(dt);

		if (FlxG.keys.justPressed.F) {
			FlxG.fullscreen = !FlxG.fullscreen;
		}
	}

	function killBoth(enemy, bullet) {
		bullet.kill();
		enemy.kill();
	}

	function killPlayer(enemy, player) {
		FlxG.switchState(new MenuState());
	}

	function createClouds(count) {
		for(x in 0...count) {
			background.add(new Cloud());
		}
	}

	function collectPowerup(powerup : Powerup, player : Player) : Bool {
		player.gun.collectPowerup();
		powerup.kill();

		return false;
	}
}
